package service;

import domain.Student;
import org.junit.jupiter.api.*;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import repository.GradeXMLRepository;
import repository.HomeworkXMLRepository;
import repository.StudentXMLRepository;
import validation.ValidationException;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.StreamSupport;

public class ServiceMockTest {

    private static final String TEST_STUDENT_ID = "test_student_id";
    private static final String TEST_HOMEWORK_ID = "test_homework_id";

    private Service service;

    private StudentXMLRepository studentRepo;
    private HomeworkXMLRepository homeworkRepo;
    private GradeXMLRepository gradeRepo;

    @BeforeEach
    private void init() {
        studentRepo = Mockito.mock(StudentXMLRepository.class);
        homeworkRepo = Mockito.mock(HomeworkXMLRepository.class);
        gradeRepo = Mockito.mock(GradeXMLRepository.class);
        service = new Service(studentRepo, homeworkRepo, gradeRepo);
    }

    @AfterEach
    public void removeTestStudent() {
        service.deleteStudent(TEST_STUDENT_ID);
        service.deleteHomework(TEST_HOMEWORK_ID);
    }

    @Test
    public void testStudentRepoSaveMethodCalledCorrectly() {
        Mockito.clearInvocations(studentRepo);
        Mockito.when(studentRepo.save(Mockito.any()))
                .thenAnswer(invocationOnMock -> invocationOnMock.getArgument(0, Student.class));

        service.saveStudent(TEST_STUDENT_ID, "TestStudent", 555);

        ArgumentCaptor<Student> studentArg = ArgumentCaptor.forClass(Student.class);
        Mockito.verify(studentRepo).save(studentArg.capture());

        Assertions.assertEquals(TEST_STUDENT_ID, studentArg.getValue().getID());
        Assertions.assertEquals("TestStudent", studentArg.getValue().getName());
        Assertions.assertEquals(555, studentArg.getValue().getGroup());
    }

    @Test
    public void testAddValidStudentWithMockedRepo() {
        List<Student> students = new ArrayList<>();
        Mockito.when(studentRepo.findAll()).thenReturn(students);
        Mockito.when(studentRepo.save(Mockito.any()))
                .thenAnswer(inv -> {
                    Student student = inv.getArgument(0, Student.class);
                    students.add(student);
                    return student;
                });

        long initialStudentsSize = StreamSupport.stream(service.findAllStudents().spliterator(), false).count();
        service.saveStudent(TEST_STUDENT_ID, "TestStudent", 555);
        long newStudentsSize = StreamSupport.stream(service.findAllStudents().spliterator(), false).count();
        Assertions.assertEquals(initialStudentsSize + 1, newStudentsSize);
    }

    @Test
    public void saveShouldFailIfRepoThrowsException() {
        Mockito.when(studentRepo.save(Mockito.any())).thenThrow(ValidationException.class);
        Assertions.assertAll("SaveStudent throws ValidationException for invalid student data!",
                () -> Assertions.assertThrows(ValidationException.class, () -> service.saveStudent(TEST_STUDENT_ID, "TestStudent", 10)),
                () -> Assertions.assertThrows(ValidationException.class, () -> service.saveStudent("", "TestStudent", 200)),
                () -> Assertions.assertThrows(ValidationException.class, () -> service.saveStudent(TEST_STUDENT_ID, "", 200)));
    }


}
