package service;

import domain.Grade;
import domain.Homework;
import domain.Student;
import org.junit.jupiter.api.*;
import repository.GradeXMLRepository;
import repository.HomeworkXMLRepository;
import repository.StudentXMLRepository;
import validation.*;

import java.util.Optional;
import java.util.stream.StreamSupport;

public class ServiceTest {

    private static final String TEST_STUDENT_ID = "test_student_id";
    private static final String TEST_HOMEWORK_ID = "test_homework_id";

    private static Service service;

    @BeforeAll
    public static void init() {
        Validator<Student> studentValidator = new StudentValidator();
        Validator<Homework> homeworkValidator = new HomeworkValidator();
        Validator<Grade> gradeValidator = new GradeValidator();

        StudentXMLRepository fileRepository1 = new StudentXMLRepository(studentValidator, "students.xml");
        HomeworkXMLRepository fileRepository2 = new HomeworkXMLRepository(homeworkValidator, "homework.xml");
        GradeXMLRepository fileRepository3 = new GradeXMLRepository(gradeValidator, "grades.xml");

        service = new Service(fileRepository1, fileRepository2, fileRepository3);
    }

    @AfterEach
    public void removeTestStudent() {
        service.deleteStudent(TEST_STUDENT_ID);
        service.deleteHomework(TEST_HOMEWORK_ID);
    }

    @Test
    public void testAddValidStudent() {
        long initialStudentsSize = StreamSupport.stream(service.findAllStudents().spliterator(), false).count();
        service.saveStudent(TEST_STUDENT_ID, "TestStudent", 555);
        long newStudentsSize = StreamSupport.stream(service.findAllStudents().spliterator(), false).count();
        Assertions.assertEquals(initialStudentsSize + 1, newStudentsSize);
    }

    @Test
    public void invalidStudentShouldReturnOne() {
        Assertions.assertAll("SaveStudent throws ValidationException for invalid student data!",
                () -> Assertions.assertThrows(ValidationException.class, () -> service.saveStudent(TEST_STUDENT_ID, "TestStudent", 10)),
                () -> Assertions.assertThrows(ValidationException.class, () -> service.saveStudent("", "TestStudent", 200)),
                () -> Assertions.assertThrows(ValidationException.class, () -> service.saveStudent(TEST_STUDENT_ID, "", 200)));
    }

    @Test
    public void testUpdateStudent() {
        service.saveStudent(TEST_STUDENT_ID, "TestStudent", 555);
        Student initialStudent = findStudent(TEST_STUDENT_ID);
        service.updateStudent(TEST_STUDENT_ID, "TestStudentUpdated", 444);
        Student updatedStudent = findStudent(TEST_STUDENT_ID);
        Assertions.assertEquals("TestStudentUpdated", updatedStudent.getName());
        Assertions.assertEquals(444, updatedStudent.getGroup());
        Assertions.assertNotEquals(initialStudent.getName(), updatedStudent.getName());
        Assertions.assertNotEquals(initialStudent.getGroup(), updatedStudent.getGroup());
    }

    @Test
    public void testDeleteHomework() {
        Assertions.assertEquals(1, service.saveHomework(TEST_HOMEWORK_ID, "Test homework", 7, 1));
        Assertions.assertEquals(1, service.deleteHomework(TEST_HOMEWORK_ID));
    }

    @Test
    public void testExtendHomeworkDeadline() {
        service.saveHomework(TEST_HOMEWORK_ID, "Test homework", 3, 1);
        service.extendDeadline(TEST_HOMEWORK_ID, 3);
        Optional<Homework> homeworkWithNewDeadline = StreamSupport.stream(service.findAllHomework().spliterator(), false)
                .filter(homework -> TEST_HOMEWORK_ID.equals(homework.getID())).findFirst();
        Assertions.assertTrue(homeworkWithNewDeadline.isPresent());
        Assertions.assertEquals(6, homeworkWithNewDeadline.get().getDeadline());
    }

    private Student findStudent(String studentId) {
        return StreamSupport.stream(service.findAllStudents().spliterator(), false)
                .filter(student -> student.getID().equals(studentId))
                .findFirst().orElse(null);
    }

}
